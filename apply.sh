#!/bin/bash

source func.sh

apply_customization() {
    PS1="$1"
}

if [ -f "config.sh" ]; then
    source config.sh
else
    echo "Erro: Arquivo 'config.sh' não encontrado."
    exit 1
fi

apply_customization "$COLOR\u@\h \W\$ \[\e[0m\]"

