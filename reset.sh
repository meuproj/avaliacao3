#!/bin/bash

source func.sh

if [ -f "config.sh" ]; then

    source config.sh
else
    echo "Erro: Arquivo 'config.sh' não encontrado."
    exit 1
fi

reset_customization

