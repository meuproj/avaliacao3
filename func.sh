#!/bin/bash

apply_customization() {
    PS1="$1"
}

reset_customization() {
    PS1="\u@\h \W\$ "
}
