#!/bin/bash

source func.sh

show_customization_options() {
	echo "escolha uma opcao de customizacao:"
	echo "1.mudança de cor"
	echo "2. exibir apenas o nome do usuario"
	echo "3.prompt em duas linhas"
	echo "4.exiba numero de arquivos no diretorio"
	echo "5.exiba a data e hora no prompt"
	echo "5.resetar para o padrao"
}

show_color_options() {
    echo "Escolha uma cor:"
    echo "1. Verde"
    echo "2. Rosa"
    echo "3. Laranja"
    echo "4. Azul"
}

apply_color() {
    case $1 in
        1)
            COLOR='\[\e[0;32m\]'  # Verde
            ;;
        2)
            COLOR='\[\e[0;35m\]'  # Rosa
            ;;
        3)
            COLOR='\[\e[0;33m\]'  # Laranja
            ;;
        4)
            COLOR='\[\e[0;34m\]'  # Azul
            ;;
        *)
            echo "Opção inválida."
            exit 1
            ;;
    esac
}

# Exibe as opções de cor
show_color_options

# Solicita ao usuário para escolher uma cor
read -p "Digite o número da cor desejada: " color_choice

# Aplica a cor escolhida ao PS1
apply_color $color_choice

# Exibe uma mensagem com a cor escolhida
echo -e "${COLOR}A cor escolhida foi aplicada.${NC}"

# Exemplo adicional: Restaura a cor para a padrão
# NC='\033[0m'  # No Color
# echo -e "${NC}A cor foi resetada para o padrão.${NC}"

